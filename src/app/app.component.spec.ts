import { TestBed } from '@angular/core/testing';
import {of} from "rxjs";

import { AppComponent } from './app.component';
import { HeaderComponent } from "./components/header/header.component";
import { ButtonComponent } from "./components/button/button.component";
import { NounsComponent } from "./components/nouns/nouns.component";
import { FooterComponent } from "./components/footer/footer.component";
import { NounService } from "./services/noun.service";

class MockNounService {
  getNouns() {
    const nouns = [{ id: 1 }];

    return of(nouns);
  }
}

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        ButtonComponent,
        NounsComponent,
        FooterComponent,
      ],
      providers: [
        NounsComponent,
        { provide: NounService, useClass: MockNounService }
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
