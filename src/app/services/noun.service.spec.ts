import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NounService } from './noun.service';
import { Noun } from "../Noun";

describe('NounService', () => {
  let httpTestingController: HttpTestingController;
  let service: NounService;

  const apiUrl: string = 'http://localhost:2000/nouns';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ NounService ],
      imports: [ HttpClientTestingModule ]
    }).compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NounService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should successfully call endpoint for getNouns()', (done) => {
    const mockNouns: Noun[] = [
      {
        id: 1,
        english: 'English word 1',
        russian: 'Russian word 1',
        gender: 'f',
        notes: 'notes 1'
      },
      {
        id: 2,
        english: 'English word 2',
        russian: 'Russian word 2',
        gender: 'm',
        notes: 'notes 2'
      }
    ];

    service.getNouns().subscribe(nouns => {
      expect(nouns).toBe(mockNouns);
      done();
    });

    const testRequest = httpTestingController.expectOne(apiUrl);

    testRequest.flush(mockNouns);
  });

  // todo: refactor
  it('should successfully call endpoint for deleteNoun()', (done) => {
    const mockNouns: Noun[] = [
      {
        id: 1,
        english: 'English word 1',
        russian: 'Russian word 1',
        gender: 'f',
        notes: 'notes 1'
      },
      {
        id: 2,
        english: 'English word 2',
        russian: 'Russian word 2',
        gender: 'm',
        notes: 'notes 2'
      }
    ];

    const id: number = 2;

    service.deleteNoun(id).subscribe(nouns => {
      expect(nouns).toEqual(mockNouns);
      done();
    });

    const testRequest = httpTestingController.expectOne(`${apiUrl}/${id}`);

    testRequest.flush(mockNouns);
  });
});
