import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { Noun } from "../Noun";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class NounService {
  private apiUrl = 'http://localhost:2000/nouns';

  constructor(private http: HttpClient) {}

  getNouns(): Observable<Noun[]> {
    return this.http.get<Noun[]>(this.apiUrl);
  }

  addNoun(noun: Noun): Observable<Noun> {
    return this.http.post<Noun>(this.apiUrl, noun, httpOptions)
      .pipe(catchError(this.errorHandler));
  }

  deleteNoun(id: number): Observable<Noun[]> {
    return this.http.delete<Noun[]>(`${this.apiUrl}/${id}`);
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
