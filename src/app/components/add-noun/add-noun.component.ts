import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Noun } from "../../Noun";

import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-add-noun',
  templateUrl: './add-noun.component.html',
  styleUrls: ['./add-noun.component.css']
})
export class AddNounComponent implements OnInit {

  getEnglishNoun() {
    return this.addNounForm.controls.english;
  }

  getRussianNoun() {
    return this.addNounForm.controls.russian;
  }

  @Input() errorMessage: string = '';
  @Output() onAddNoun: EventEmitter<Noun> = new EventEmitter<Noun>();
  english: string = '';
  russian: string = '';
  notes: string = '';
  gender: string = '';

  constructor(private formBuilder: FormBuilder) {}

  addNounForm = this.formBuilder.group({
    english: ['', [Validators.required, Validators.maxLength(50)]],
    russian: ['', [Validators.required, Validators.maxLength(50)]],
    gender: ['', Validators.required],
    notes: ['', Validators.maxLength(100)],
  });

  ngOnInit(): void {
  }

  onSubmit() {
    const newNoun = {
      id: 0,
      english: this.addNounForm.value.english,
      russian: this.addNounForm.value.russian,
      notes: this.addNounForm.value.notes,
      gender: this.addNounForm.value.gender,
    }

    this.onAddNoun.emit(newNoun);

    this.english = '';
    this.russian = '';
    this.gender = '';
    this.notes = '';
  }
}
