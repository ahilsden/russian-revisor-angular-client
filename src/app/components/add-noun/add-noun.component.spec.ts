import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNounComponent } from './add-noun.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

describe('AddNounComponent', () => {
  let component: AddNounComponent;
  let fixture: ComponentFixture<AddNounComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AddNounComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // todo: split tests up
  // Test: form validity, input validity, input errors

  it('should test form validity', () => {
    const form = component.addNounForm;

    expect(form.valid).toBeFalsy();

    const nounLessThanFiftyCharacters = 'Noun';
    const nounGreaterThanFiftyCharacters = 'NounNounNounNounNounNounNounNounNounNounNounNounNoun';
    const gender = 'f';
    const notesLessThanFiftyCharacters = 'Notes';
    const notesGreaterThanFiftyCharacters = 'NotesNotesNotesNotesNotesNotesNotesNotesNotesNotes' +
      'NotesNotesNotesNotesNotesNotesNotesNotesNotesNotesNotesNotes';

    form.controls.english.setValue(nounGreaterThanFiftyCharacters);
    form.controls.russian.setValue(nounLessThanFiftyCharacters);
    form.controls.gender.setValue(gender);
    form.controls.notes.setValue(notesLessThanFiftyCharacters);

    expect(form.valid).toBeFalsy();

    form.controls.english.setValue(nounLessThanFiftyCharacters);
    form.controls.russian.setValue(nounGreaterThanFiftyCharacters);

    expect(form.valid).toBeFalsy();

    form.controls.english.setValue(nounLessThanFiftyCharacters);
    form.controls.russian.setValue(nounLessThanFiftyCharacters);
    form.controls.gender.setValue(null);

    expect(form.valid).toBeFalsy();

    form.controls.gender.setValue(gender);
    form.controls.notes.setValue(notesGreaterThanFiftyCharacters);

    expect(form.valid).toBeFalsy();

    form.controls.notes.setValue(notesLessThanFiftyCharacters);

    expect(form.valid).toBeTruthy();
  });
});
