import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Noun } from "../../Noun";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  @Output() btnClick: EventEmitter<Noun> = new EventEmitter();
  @Input() color: string = '';
  @Input() text: string = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  onClick() {
    this.btnClick.emit();
  }
}
