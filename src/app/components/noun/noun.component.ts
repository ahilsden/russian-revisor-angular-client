import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { Noun } from "../../Noun";

@Component({
  selector: 'app-noun',
  templateUrl: './noun.component.html',
  styleUrls: ['./noun.component.css']
})
export class NounComponent implements OnInit {
  @Input() noun: Noun = <Noun>{};
  @Output() onDeleteNoun: EventEmitter<number> = new EventEmitter<number>();
  faTimes = faTimes;

  constructor() { }

  ngOnInit(): void {}

  onDelete(id: number): void {
    this.onDeleteNoun.emit(id);
  }
}
