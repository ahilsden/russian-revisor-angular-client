import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NounComponent } from './noun.component';

const mockNoun = {
  id: 1,
  english: 'English word',
  russian: 'Russian word',
  gender: 'f',
  notes: 'notes',
}

// todo: noun service and addNoun tests
describe('NounComponent', () => {
  let component: NounComponent;
  let fixture: ComponentFixture<NounComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NounComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly render the table values',  () => {
    component.noun = mockNoun;

    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const nodes = compiled.querySelectorAll('td');

    expect(nodes[0].textContent).toBe('English word');
    expect(nodes[1].textContent).toBe('Russian word');
    expect(nodes[2].textContent).toBe('notes');
  });


});
