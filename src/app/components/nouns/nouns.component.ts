import { Component, OnInit } from '@angular/core';
import { Noun } from "../../Noun";
import { NounService } from "../../services/noun.service";

@Component({
  selector: 'app-nouns',
  templateUrl: './nouns.component.html',
  styleUrls: ['./nouns.component.css']
})
export class NounsComponent implements OnInit {
  nouns: Noun[] = [];
  errorMessage: string = '';

  constructor(private nounService: NounService) { }

  ngOnInit(): void {
    this.nounService.getNouns()
      .subscribe(nouns => this.nouns = nouns);
  }

  addNoun(noun: Noun) {
    this.nounService.addNoun(noun)
      .subscribe(
        data => this.nouns.push(data),
        error => this.errorMessage = error.statusText
    );
  }

  deleteNoun(id: number) {
    this.nounService.deleteNoun(id)
      .subscribe(() => {
        this.nouns = this.nouns.filter(nounItem => nounItem.id !== id);
      });
  }
}
