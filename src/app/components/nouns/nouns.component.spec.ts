import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from "rxjs";

import { NounsComponent } from './nouns.component';
import { NounService } from "../../services/noun.service";
import { Noun } from "../../Noun";

import { AddNounComponent } from "../add-noun/add-noun.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

class MockNounService {
  getNouns() {
    const nouns = [
      { id: 1, english: 'English word 1', russian: 'Russian word 1', gender: 'f', notes: 'notes 1'},
      { id: 2, english: 'English word 2', russian: 'Russian word 2', gender: 'm', notes: 'notes 2' }
    ];

    return of(<Noun[]>nouns);
  }

  addNoun(noun: Noun) {
    return of(<Noun>noun);
  }

  deleteNoun(id: number) {
    return of(<Noun>{});
  }
}

describe('NounsComponent', () => {
  let component: NounsComponent;
  let fixture: ComponentFixture<NounsComponent>;
  let httpTestingController: HttpTestingController;
  let service: NounService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NounsComponent,
        AddNounComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        NounsComponent,
        { provide: NounService, useClass: MockNounService }
      ]
    })
    .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NounService);

    fixture = TestBed.createComponent(NounsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly render the table headers', () => {
    const compiled = fixture.debugElement.nativeElement;
    const nodes = compiled.querySelectorAll('th');

    expect(nodes[0].textContent).toBe('English');
    expect(nodes[1].textContent).toBe('Russian');
    expect(nodes[2].textContent).toBe('Notes');
  });

  it('should add a new noun', () => {
    const numOfNounsBeforeAddNoun = component.nouns.length;

    component.addNoun(
      {
        id: 3,
        english: 'English word 3',
        russian: 'Russian word 3',
        gender: 'f',
        notes: 'notes 3'
      }
    );

    const numOfNounsAfterAddNoun = component.nouns.length;

    expect(numOfNounsAfterAddNoun).toEqual(numOfNounsBeforeAddNoun + 1);
  });

  it('should delete a noun', () => {
    const numOfNounsBeforeDeleteNoun = component.nouns.length;

    component.deleteNoun(1);

    const numOfNounsAfterDeleteNoun = component.nouns.length;

    expect(numOfNounsAfterDeleteNoun).toEqual(numOfNounsBeforeDeleteNoun - 1);
  });
});
