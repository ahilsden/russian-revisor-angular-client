export interface Noun {
  id: number,
  english: string,
  russian: string,
  notes: string,
  gender: string
}
